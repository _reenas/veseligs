﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Effects : MonoBehaviour
{

	public GameObject statisticsPanel;
	public GameObject statisticsButon;

	public GameObject optionsPanel;
	public GameObject optionsButon;

	public GameObject zoomSlider;

	public void changeStatistics ()
	{
		if (!statisticsPanel.activeSelf)
		{
			statisticsButon.transform.GetChild (0).gameObject.GetComponent<Text> ().text = "Aizvērt";

			optionsButon.SetActive (false);
			zoomSlider.SetActive (false);

			statisticsPanel.SetActive (true);
		}
		else if (statisticsPanel.activeSelf)
		{
			statisticsButon.transform.GetChild (0).gameObject.GetComponent<Text> ().text = "Atvērt progresu";
			statisticsPanel.SetActive (false);

			optionsButon.SetActive (true);
			zoomSlider.SetActive (true);
		}
	}

	public void changeOptions ()
	{
		if (!optionsPanel.activeSelf)
		{	
			statisticsButon.SetActive (false);
			optionsButon.SetActive (false);
			
			zoomSlider.SetActive (false);
			
			optionsPanel.SetActive (true);
		}
		else if (optionsPanel.activeSelf)
		{	
			statisticsButon.SetActive (true);
			optionsPanel.SetActive (false);

			zoomSlider.SetActive (true);
			optionsButon.SetActive (true);
			
			
		}
	}
}