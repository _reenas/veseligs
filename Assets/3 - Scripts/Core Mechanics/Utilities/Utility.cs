﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Networking;

public static class Utility
{

    public static IEnumerator json_parser(string _url, System.Action<string> _return_text)
    {
        string parsed_url = Path.Combine(Application.streamingAssetsPath, _url);
        string return_text = "";

        if (parsed_url.Contains("://") || parsed_url.Contains(":///"))
        {
            UnityWebRequest request = UnityWebRequest.Get(parsed_url);
            yield return request.SendWebRequest();
            // WWW parsed_from_server = new WWW (parsed_url);
            // yield return return_text;
            return_text = request.downloadHandler.text;
        }
        else if (File.Exists(parsed_url))
        {
            string json_data = File.ReadAllText(parsed_url);
            return_text = json_data;
        }
        else
        {
            Debug.LogWarning("File doesn't exist");
        }
        _return_text(return_text);
    }

    public static IEnumerator play_text(Text text_component, string playable_text)
    {
        Text _text_component = text_component;
        _text_component.text = " ";
        for (int _character = 0; _character < playable_text.Length; _character++)
        {
            _text_component.text += playable_text[_character];
            yield return new WaitForSeconds(0.01f);
        }
    }

    public static void Switch_Active_Object(GameObject object_to_switch)
    {
        if (object_to_switch != null)
        {
            if (object_to_switch.activeSelf == true)
            {
                object_to_switch.SetActive(false);
            }
            else
            {
                object_to_switch.SetActive(true);
            }
        }
        else
        {
            Debug.LogWarning("Cant switch. Object doesn't exist");
        }

    }

    public static List<GameObject> FindObjectsOffLayer(int layer_id)
    {
        GameObject[] layerArray = Object.FindObjectsOfType<GameObject>();
        List<GameObject> layerList = new List<GameObject>();
        for (int i = 0; i < layerArray.Length; i++)
        {
            if (layerArray[i].layer == layer_id)
            {
                layerList.Add(layerArray[i]);
            }
        }

        if (layerList.Count == 0)
        {
            return null;
        }

        return layerList;
    }
}

// public static void Switch_Avatar (int _number, GameObject player_object) {
// 	int character_number = _number;
// 	Avatar[] player_avatars = player_object.GetComponent<PlayerController> ().playerAvatars;
// 	Animator player_animator = player_object.GetComponent<Animator> ();
// 	player_animator.avatar = player_avatars[character_number];

// 	if (character_number == 0) {
// 		player_object.transform.GetChild (0).gameObject.SetActive (true);
// 		player_object.transform.GetChild (1).gameObject.SetActive (false);
// 	} else {
// 		player_object.transform.GetChild (0).gameObject.SetActive (false);
// 		player_object.transform.GetChild (1).gameObject.SetActive (true);
// 	}

// 	player_animator.Rebind ();
// }g