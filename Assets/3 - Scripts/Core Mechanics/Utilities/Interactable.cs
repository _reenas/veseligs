﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {
    bool isFocus = false;
    public float radius = 3f;
    public Transform interactionTransform;
    bool hasInteracted = false;
    public Transform player;
    public string theme;

    public virtual void Interact () {
        // Debug.Log("Interacting with: " + transform.name);
        //Open question panel n shit here

        GameManager.instance.canInteract = true;
        GameManager.instance.questionLoaded = true;
        GameManager.instance.canInteract = true;
    }

    void Start () {
        theme = tag;
    }

    void Update () {
        if (isFocus && !hasInteracted) {
            float distance = Vector3.Distance (player.position, interactionTransform.position);
            if (distance <= radius) {
                Interact ();

                hasInteracted = true;
            }
        }
        if (GameManager.instance.blockedThemesAndQuestions[theme].Count >= GameManager.instance.maxQuestionsPerTheme) {
            transform.Find ("Izsaukuma").gameObject.SetActive (false);
        }

    }

    public void OnFocused (Transform playerTransform) {
        isFocus = true;
        player = playerTransform;
        hasInteracted = false;
    }
    public void OnDeFocused () {
        isFocus = false;
        player = null;
        hasInteracted = false;
    }

    void OnDrawGizmosSelected () {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere (interactionTransform.position, radius);
    }
}