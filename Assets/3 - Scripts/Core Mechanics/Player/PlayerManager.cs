﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

[RequireComponent (typeof (PlayerMotor))]
public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance;
    public Animator playerAnimator;
    [Header ("Movement and Interaction")]
    public bool can_move_player = true;
    public float speed;
    public Interactable focus;
    public GameObject player;
    public PlayerMotor motor;
    public LayerMask movementMask;
    public LayerMask interactableMask;
    public GameObject _marker;
    [Header ("UI")]
    public bool can_open_ui = false;
    public GameObject[] canvasPanels;
    [Header ("Other")]
    Camera cam;
    public TrafficSystem TS;
    public Avatar[] avatars;
    public RuntimeAnimatorController[] controllers;

    void Start ()
    {
        if (instance == null)
        {
            instance = this;
        }

        if (GameManager.instance.selected_character == 0)
        {
            this.gameObject.transform.GetChild (0).gameObject.SetActive (true);
            this.gameObject.transform.GetChild (1).gameObject.SetActive (false);
            playerAnimator.avatar = avatars[0];
            playerAnimator.runtimeAnimatorController = controllers[0];
        }
        else if (GameManager.instance.selected_character == 1)
        {
            this.gameObject.transform.GetChild (0).gameObject.SetActive (false);
            this.gameObject.transform.GetChild (1).gameObject.SetActive (true);
            playerAnimator.avatar = avatars[1];
            playerAnimator.runtimeAnimatorController = controllers[1];
        }
        player = GameObject.Find ("Player");
        cam = Camera.main;
        motor = GetComponent<PlayerMotor> ();
        _marker.SetActive (false);
        Scene curScene = SceneManager.GetActiveScene ();

        if (curScene.name == "CITY_LEVEL")
        {
            TS = GameObject.Find ("TraficManager").GetComponent<TrafficSystem> ();
        }
        else
        {
            TS = null;
        }
    }

    void Update ()
    {

        cam.transform.position = this.transform.position;
        if (!IsPointerOverUIObject())
        {
            if (Input.GetMouseButtonDown (0) && !GameManager.instance.questionLoaded && !canvasPanels[0].activeSelf)
            {
                Ray ray = cam.ScreenPointToRay (Input.mousePosition);
                RaycastHit hit;
                int ignoreLayer = 1 << 27;
                ignoreLayer = ~ignoreLayer;
                if (Physics.Raycast (ray, out hit, 100, ignoreLayer))
                {
                    Debug.Log (hit.transform.name);
                    can_open_ui = false;
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer ("Ground"))
                    {
                        StartCoroutine (Marker ());
                        _marker.transform.position = hit.point + new Vector3 (0f, 0.1f, 0f);
                        motor.MoveToPoint (hit.point);
                        RemoveFocus ();
                        Debug.DrawLine (this.transform.position, hit.point, Color.red, 2f);
                    }

                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer ("Interactables"))
                    {
                        _marker.SetActive (false);
                        Interactable interactable = hit.collider.GetComponent<Interactable> ();
                        if (interactable != null)
                        {
                            SetFocus (interactable);
                        }
                        Debug.DrawLine (this.transform.position, hit.point, Color.red, 2f);
                    }

                }
            }
        }
    }

    private bool IsPointerOverUIObject ()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData (EventSystem.current);
        eventDataCurrentPosition.position = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult> ();
        EventSystem.current.RaycastAll (eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    IEnumerator Marker ()
    {
        _marker.SetActive (true);
        yield return new WaitForSeconds (0.3f);
        _marker.SetActive (false);
    }

    public void SetFocus (Interactable newFocus)
    {
        if (newFocus != focus)
        {
            if (focus != null)
            {
                focus.OnDeFocused ();
            }
            motor.FollowTarget (newFocus);
            focus = newFocus;
        }
        newFocus.OnFocused (transform);
    }

    public void RemoveFocus ()
    {
        if (focus != null)
        {
            focus.OnDeFocused ();
        }
        focus = null;
        motor.StopFollowingTarget ();
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.tag == "Room")
        {
            RoomManager.instance.RoomCheck (other.gameObject);
        }
        if (other.gameObject.tag == "Block")
        {
            CityManager.instance.BlockCheck (other.gameObject);
        }
        if (other.transform.tag == "Cross1")
        {
            TS.playerCrossed = false;
        }
        if (other.transform.tag == "Cross2")
        {
            TS.playerCrossed = true;
        }
    }
}