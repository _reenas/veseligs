﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class TutorialSlide {

    public Sprite bacgroundImage;
    public Sprite[] girlImage;
}

public class MainMenuManager : MonoBehaviour
{

    
    // Get this from game manager
    public GameObject player;

    // Arrows for character switching
    public Button[] arrows;

    // Add these in panels and array
    public Button start_game;
    public Button tutorial;
    public Button options;

    public int character_selected;

    public Sprite[] tutorialSprt;
    public Image tutorialMainImg;
    public GameObject tutorialPanel;
    public Button tutNext1;
    public int nr = 0;
    public Slider volumeSlider;
    public GameObject optionsPanel;
    public GameObject menuPanel;
    public Button closeOptions;

    // Use this for initialization
    void Start()
    {
        tutorialPanel.SetActive(false);

        for (int i = 0; i < arrows.Length; i++) {
            arrows[i].onClick.AddListener (() => {
                ChangeCharacter ();
            });
        }

        start_game.onClick.AddListener(() =>
        {
            GameManager.instance.changeScene("HOUSE_LEVEL", "House/");
        });

        tutorial.onClick.AddListener(() =>
        {
            tutorialPanel.SetActive(true);

        });

        tutNext1.onClick.AddListener(() =>
        {
            Tutorial();

        });
        closeOptions.onClick.AddListener(() =>
        {
            CloseOptions();
        });
        options.onClick.AddListener(() =>
        {
            OpenOptions();
        });
    }

    // Update is called once per frame
    void Update()
    {
        GameManager.instance.selected_character = character_selected;
        SetVolume();
    }

    public void ChangeCharacter()
    {
        int character_number = GameManager.instance.selected_character;

        if (character_number == 0)
        {
            character_number = 1;
            player.transform.GetChild(0).gameObject.SetActive(true);
            player.transform.GetChild(1).gameObject.SetActive(false);

        }
        else if (character_number == 1)
        {
            character_number = 0;
            player.transform.GetChild(0).gameObject.SetActive(false);
            player.transform.GetChild(1).gameObject.SetActive(true);
        }
        character_selected = character_number;
    }

    void Tutorial()
    {
        if (nr == 10)
        {
            nr = -1;
            tutorialPanel.SetActive(false);
        }
        nr++;
        tutorialMainImg.sprite = tutorialSprt[nr];
    }
    public void SetVolume()
    {
        AudioSource audio = GameManager.instance.gameObject.GetComponent<AudioSource>();
        audio.volume = volumeSlider.value;
    }

    void CloseOptions()
    {
        optionsPanel.SetActive(false);
        menuPanel.SetActive(true);
    }

    void OpenOptions()
    {
        optionsPanel.SetActive(true);
        menuPanel.SetActive(false);
    }
}