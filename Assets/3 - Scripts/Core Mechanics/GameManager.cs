﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    // GameManager Instance
    public static GameManager instance;

    // Stores data witch themes and questions have been answered

    public Dictionary<string, List<int>> blockedThemesAndQuestions = new Dictionary<string, List<int>> ();
    public int maxQuestionsPerTheme = 5;
    // stores how much questions have been answered (total score)
    public int totalAnsweredQuestions = 0;
    public int totalAllowedQuestions = 0;
    public float totalScore = 0f;
    public string currentTheme = "";
    public int currentQuestionID = 0;
    public bool questionLoaded = false;
    public bool canInteract = false;

    public int selected_character;
    public List<Theme> backup = new List<Theme> ();
    public List<Theme> Themes = new List<Theme> ();
    public List<GameObject> all_statistics = new List<GameObject> ();

    [SerializeField]
    public string loaded_text = "";

    [Header ("Loadable file")]
    public string File;

    public GameObject endPanel;
    public GameObject statisticPrefab;
    public GameObject statsContainer;

    // load themes and questions or json files
    IEnumerator pre_load () {
        yield return StartCoroutine (Utility.json_parser (File, value => loaded_text = value));
    }

    // Creates themes and questions from json paresd file
    public void Load_Themes (string json_text) {
        Quiz_List quiz_questions = JsonUtility.FromJson<Quiz_List> (json_text);

        Themes.Clear();
        for (int i = 0; i < quiz_questions.themes.Length; i++) {
            Themes.Add (quiz_questions.themes[i]);
        }
        
    }
    void Awake () {

        if (instance == null) {
            instance = this;
            DontDestroyOnLoad (this.gameObject);
            StartCoroutine ("pre_load");

            return;
        } else {
            Destroy (this.gameObject);
        }

    }

    void Start () {
        Load_Themes (loaded_text);

        for (int i = 0; i < Themes.Count; i++) {
            if (!GameManager.instance.blockedThemesAndQuestions.ContainsKey (Themes[i].name)) {
                GameManager.instance.blockedThemesAndQuestions.Add (Themes[i].name, new List<int> ());
            }
            GameObject current_stat_prefab = Instantiate (statisticPrefab, statsContainer.transform);
            current_stat_prefab.transform.name = Themes[i].name;
            current_stat_prefab.transform.GetChild (0).GetComponent<Text> ().text = Themes[i].name;
            current_stat_prefab.transform.GetChild (1).GetComponent<Text> ().text = "0%";
            current_stat_prefab.transform.GetChild (2).GetComponent<Slider> ().value = 0;

            all_statistics.Add (current_stat_prefab);

        }
    }

    public void changeScene (string scene_name, string scene_location = "1 - Scenes/") {
        string default_scene_lcoation = "1 - Scenes/";
        if (default_scene_lcoation == scene_location) {
            SceneManager.LoadSceneAsync (scene_location + scene_name);
        } else {
            SceneManager.LoadSceneAsync (default_scene_lcoation + scene_location + scene_name);
        }
    }

    public void Create_Statistics (GameObject panel, GameObject prefab) {

        for (int current_stat = 0; current_stat < all_statistics.Count; current_stat++) {
            // theme panel
            RectTransform rt_panel = panel.transform.GetChild (0).GetComponent<RectTransform> ();
            rt_panel.GetComponent<GridLayoutGroup> ().cellSize = new Vector2 ((rt_panel.rect.width / 2), (rt_panel.rect.height / 7));

            GameObject current_stat_prefab = all_statistics.ElementAt (current_stat);
            current_stat_prefab.transform.SetParent (panel.transform.GetChild (0).transform, false);

            current_stat_prefab.SetActive (true);
            current_stat_prefab.transform.GetChild (0).GetComponent<Text> ().text = Themes[current_stat].name;
            current_stat_prefab.transform.GetChild (1).GetComponent<Text> ().text = all_statistics.ElementAt (current_stat).transform.GetChild (1).GetComponent<Text> ().text;
            current_stat_prefab.transform.GetChild (2).GetComponent<Slider> ().value = all_statistics.ElementAt (current_stat).transform.GetChild (2).GetComponent<Slider> ().value;

            Transform total_panel = panel.transform.GetChild (1);
            total_panel.GetChild (0).transform.GetChild (0).GetComponent<Image> ().fillAmount = totalScore;
        }
    }

    public void makeStats (GameObject prefab) {
        all_statistics.Clear ();
        for (int i = 0; i < Themes.Count; i++) {
            if (!GameManager.instance.blockedThemesAndQuestions.ContainsKey (Themes[i].name)) {
                GameManager.instance.blockedThemesAndQuestions.Add (Themes[i].name, new List<int> ());
            }
            GameObject current_stat_prefab = Instantiate (statisticPrefab, statsContainer.transform);
            current_stat_prefab.transform.name = Themes[i].name;
            current_stat_prefab.transform.GetChild (0).GetComponent<Text> ().text = Themes[i].name;
            current_stat_prefab.transform.GetChild (1).GetComponent<Text> ().text = "0%";
            current_stat_prefab.transform.GetChild (2).GetComponent<Slider> ().value = 0;

            all_statistics.Add (current_stat_prefab);

        }
    }
}