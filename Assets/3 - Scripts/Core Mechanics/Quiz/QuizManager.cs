﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QuizManager : MonoBehaviour
{

    public static QuizManager instance;

    // ------ PRIVATE VARIABLES ------
    private Animator quizAnimator;
    // Raycast for interactables
    private Ray quiz_raycast;
    private RaycastHit raycast_hit;

    // Loaded theme reference
    private List<Theme> Themes = new List<Theme> ();

    // Info about curent interactable
    private string themeName = "";
    private int themeId = 0;
    private int questionId = 0;
    private string currentQuestionAnswer = "";
    public Question currentQuestion;

    // Random answers saved
    public List<GameObject> rand_answers = new List<GameObject> ();
    // Returned file
    // private string returned_text = " ";

    // ------ PUBLIC VARIABLES ------
    [Header ("Question button sprites")]
    public Sprite[] resources;

    [Header ("Quiz UI objects")]
    public GameObject[] Panels;
    public GameObject[] answerButtons = new GameObject[3];

    [Header ("Question buttons")]
    public GameObject exitQuestionButton;
    public GameObject hintButton;
    public GameObject nextQuestionButton;
    public GameObject cityButton;
    public GameObject optionsButton;

    [Header ("Blocked List")]
    public List<Theme> blockedThemes = new List<Theme> ();

    [Header ("Progress bar preafb")]
    public GameObject statistic_prefab;

    [Header ("List of theme progress bars")]
    public List<GameObject> all_statistics;

    [Header ("Interactable color")]
    public Color interactableColor;

    [Header ("All interactables in scene")]
    public List<GameObject> allInteractables = new List<GameObject> ();

    [Header ("Currently selected interactable")]
    public GameObject selectedObject;
    public string selectedObjectTheme;

    [Header ("Pressed buttons")]
    public bool questionLoaded = false;
    public bool hintPressed = false;
    public bool answerPressed = false;
    public bool nextQuestionPressed = false;

    public Scene currentScene;

    public Slider zoomSlider;
    public Camera cameraZoom;

    public bool test = false;
    void Awake ()
    {
        quizAnimator = Panels[0].GetComponent<Animator> ();
    }

    void Start ()
    {
        instance = this;
        currentScene = SceneManager.GetActiveScene ();

        for (int i = 0; i < answerButtons.Length; i++)
        {
            Image changeable = answerButtons[i].GetComponent<Image> ();
            changeable.sprite = resources[0];
        }

        GameManager.instance.questionLoaded = false;
        hintPressed = false;
        answerPressed = false;
        nextQuestionPressed = false;
        quizAnimator.ResetTrigger ("Load Question");

        // Create list of thems for droping them out
        Themes = GameManager.instance.Themes;
        blockedThemes = Themes;

        if (GameManager.instance.selected_character == 0)
        {
            Panels[8].transform.GetChild (0).gameObject.SetActive (true);
            Panels[8].transform.GetChild (1).gameObject.SetActive (false);
        }
        else if (GameManager.instance.selected_character == 1)
        {
            Panels[8].transform.GetChild (0).gameObject.SetActive (false);
            Panels[8].transform.GetChild (1).gameObject.SetActive (true);
        }

        GameManager.instance.Create_Statistics (Panels[5], statistic_prefab);

        exitQuestionButton.GetComponent<Button> ().onClick.AddListener (() =>
        {
            exitQestion ();
        });

        hintButton.GetComponent<Button> ().onClick.AddListener (() =>
        {
            hintPanelActive ();
        });

        nextQuestionButton.GetComponent<Button> ().onClick.AddListener (() =>
        {
            nextQuestionPressed = true;
            nextQuestionButton.GetComponent<Button> ().interactable = false;
            
        });

        if (currentScene.name == "CITY_LEVEL")
        {
            cityButton.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "Doties uz dzīvokli";
        }
        if (currentScene.name == "HOUSE_LEVEL")
        {
            cityButton.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "Doties uz pilsētu";
        }

        cityButton.GetComponent<Button> ().onClick.AddListener (() =>
        {
            changeStatParent ();
            if (GameManager.instance.statsContainer.transform.childCount == 14)
            {
                if (currentScene.name == "HOUSE_LEVEL")
                {
                    GameManager.instance.changeScene ("CITY_LEVEL", "City/");
                }
                else if (currentScene.name == "CITY_LEVEL")
                {
                    GameManager.instance.changeScene ("HOUSE_LEVEL", "House/");
                }
            }
        });

    }

    public void restartThemes ()
    {
        // blockedThemes = GameManager.instance.backupThemes;
    }

    void changeStatParent ()
    {
        int childs = Panels[5].transform.GetChild (0).childCount;

        while (Panels[5].transform.GetChild (0).childCount != 0)
        {
            childs -= 1;
            Panels[5].transform.GetChild (0).GetChild (childs).transform.SetParent (GameManager.instance.statsContainer.transform, false);
        }
    }

    public void exitQestion ()
    {

        if (answerPressed == false)
        {
            quizAnimator.SetTrigger ("Exit Question");
        }

        if (quizAnimator.GetCurrentAnimatorStateInfo (0).IsName ("Load Fact"))
        {
            quizAnimator.SetTrigger ("Exit Fact Only");
        }
        else
        {
            quizAnimator.SetTrigger ("Exit Question");
        }

        for (int i = 0; i < answerButtons.Length; i++)
        {
            Image changeable = answerButtons[i].GetComponent<Image> ();
            changeable.sprite = resources[0];
        }

        for (int i = 0; i < Panels[3].transform.childCount; i++)
        {
            Panels[3].transform.GetChild (i).gameObject.GetComponent<CanvasGroup> ().alpha = 0f;
        }
        StopCoroutine ("Draw_Effect_Panel");
        Panels[3].SetActive (false);
        GameManager.instance.questionLoaded = false;
        hintPressed = false;
        answerPressed = false;
        nextQuestionPressed = false;
        quizAnimator.ResetTrigger ("Load Question");
    }

    void hintPanelActive ()
    {
        // Check only for one shown hint per question
        if (hintPressed == false)
        {
            hintPressed = true;
            // If only if hint panel isnt active allready
            if (!Panels[3].activeSelf)
            {
                // Set hint panel to active
                Panels[3].SetActive (true);
            }
        }
    }

    void Update ()
    {

        SetZoom ();

        questionLoaded = GameManager.instance.questionLoaded;

        if (Panels[3].activeSelf)
        {
            StartCoroutine (Draw_Effect_Panel (Panels[3], .7f, 2f));
        }
        else
        {
            StopCoroutine ("Draw_Effect_Panel");
            for (int i = 0; i < Panels[3].transform.childCount; i++)
            {
                Panels[3].transform.GetChild (i).gameObject.GetComponent<CanvasGroup> ().alpha = 0f;
            }
        }
        if (answerPressed == true)
        {
            // Debug.Log ("zval");
            for (int i = 0; i < Panels[3].transform.childCount; i++)
            {
                Panels[3].transform.GetChild (i).gameObject.GetComponent<CanvasGroup> ().alpha = 0f;
            }

            Panels[3].SetActive (false);
        }

        if (questionLoaded)
        {
            optionsButton.transform.GetChild (0).gameObject.SetActive (false);
            zoomSlider.gameObject.SetActive (false);
            Panels[5].transform.parent.GetChild (1).GetChild (0).gameObject.SetActive (false);
            Panels[5].SetActive (false);
            Panels[10].SetActive (false);

        }
        else if (quizAnimator.GetCurrentAnimatorStateInfo (0).IsName ("Idle") && !questionLoaded && !Panels[5].activeSelf && !Panels[10].activeSelf)
        {
            optionsButton.transform.GetChild (0).gameObject.SetActive (true);
            zoomSlider.gameObject.SetActive (true);
            Panels[5].transform.parent.GetChild (1).GetChild (0).gameObject.SetActive (true);
        }

        if (nextQuestionPressed == true)
        {
            next_question ();
        }

        // Update raycast
        quiz_raycast = Camera.main.ScreenPointToRay (Input.mousePosition);

        // Raycasts ray to get interactable object
        if (Physics.Raycast (quiz_raycast, out raycast_hit, Mathf.Infinity, LayerMask.GetMask ("Interactables")) && !questionLoaded)
        {
            selectedObject = raycast_hit.transform.gameObject;
            selectedObjectTheme = selectedObject.tag;
        }

        if (questionLoaded && GameManager.instance.canInteract)
        {
            Interactable_Clicked ();
            // remove current selected object
            selectedObject = null;
            GameManager.instance.canInteract = false;
        }

        if (quizAnimator.GetCurrentAnimatorStateInfo (0).IsName ("Idle") && questionLoaded)
        {
            if (GameManager.instance.blockedThemesAndQuestions[selectedObjectTheme].Count >= GameManager.instance.maxQuestionsPerTheme)
            {
                quizAnimator.SetBool ("Load Fact Only", true);
            }
            else
            {
                quizAnimator.SetBool ("Load Question", true);
            }

        }

        if (test)
        {
            GameManager.instance.Create_Statistics (Panels[5], statistic_prefab);
        }
    }

    void Interactable_Clicked ()
    {
        // If no max questions per theme show new theme
        if (GameManager.instance.blockedThemesAndQuestions[selectedObjectTheme].Count < GameManager.instance.maxQuestionsPerTheme)
        {
            nextQuestionButton.transform.GetChild (0).GetComponent<Text> ().text = "Nākamais jautājums";
            // Get random question from selected theme
            Question test_question = Get_Question (Get_Theme (selectedObjectTheme));
            // Create question
            Create_Question (test_question);
            exitQuestionButton.SetActive (true);
            quizAnimator.SetTrigger ("Load Question");

        }
        else
        {
            nextQuestionButton.GetComponent<Button> ().interactable = true;
            // If max question per theme is full show only fact
            Question test_question = Get_Question (Get_Theme (selectedObjectTheme));
            Text text_field = Panels[6].transform.GetChild (1).GetComponent<Text> ();

            exitQuestionButton.SetActive (false);

            while (test_question.fact == "")
            {
                test_question = Get_Question (Get_Theme (selectedObjectTheme));
            }

            nextQuestionButton.transform.GetChild (0).GetComponent<Text> ().text = "Aizvērt";
            text_field.text = test_question.fact;

            quizAnimator.SetTrigger ("Load Fact Only");
        }
    }

    // Get chosen theme
    Theme Get_Theme (string theme_name)
    {
        Theme _theme = new Theme ();
        for (int i = 0; i < Themes.Count; i++)
        {
            if (theme_name == Themes.ElementAt (i).name)
            {
                _theme = Themes.ElementAt (i);
                themeId = i;
                break;
            }
        }
        // store theme name to global variable
        themeName = _theme.name;
        // Save this in game manager
        GameManager.instance.currentTheme = _theme.name;
        // Return selected theme
        return _theme;
    }

    Question Get_Question (Theme theme)
    {
        Panels[7].transform.GetChild (0).GetComponent<Text> ().text = theme.name;
        // Question saved for return
        Question _question = Check_Question (themeId);

        // Store active question id to global variable
        questionId = _question.id;
        // Save this in game manager
        GameManager.instance.currentQuestionID = _question.id;
        currentQuestion = _question;
        // Return chosen question
        return _question;
    }

    void Create_Question (Question question)
    {
        hintPressed = false;
        answerPressed = false;
        nextQuestionPressed = false;
        questionLoaded = true;
        exitQuestionButton.GetComponent<Button> ().interactable = true;

        for (int i = 0; i < answerButtons.Length; i++)
        {
            answerButtons[i].GetComponent<Button> ().interactable = true;
            Image changeable = answerButtons[i].GetComponent<Image> ();
            changeable.sprite = resources[0];
        }

        // Question text filling effect
        Text text_field = Panels[1].transform.GetChild (0).GetComponent<Text> ();
        // Play text effect
        text_field.text = question.question;

        // Store right answer
        currentQuestionAnswer = question.answers[question.right_answer];

        Text counter_text = Panels[4].transform.GetChild (0).GetComponent<Text> ();

        counter_text.text = GameManager.instance.blockedThemesAndQuestions[themeName].Count.ToString ();

        // Answer creator
        Create_Answers (question);
        // Add button functionality
        Create_Answer_Function ();
    }

    Question Check_Question (int _theme_id)
    {
        // Return value
        Question whitelisted = new Question ();
        List<Question> temp = blockedThemes[_theme_id].questions.ToList ();

        int question_id = UnityEngine.Random.Range (0, blockedThemes[_theme_id].questions.Length);

        whitelisted = blockedThemes[_theme_id].questions[question_id];

        return whitelisted;
    }

    void Create_Answers (Question question)
    {
        // Fill answer button text with answers
        for (int i = 0; i < answerButtons.Length; i++)
        {
            answerButtons[i].transform.GetChild (0).GetComponent<Text> ().text = question.answers[i];
        }

        // Randomize and refresh answers
        rand_answers = Randomize_Answers (answerButtons);

        string text1 = rand_answers.ElementAt (0).transform.GetChild (0).GetComponent<Text> ().text;
        string text2 = rand_answers.ElementAt (1).transform.GetChild (0).GetComponent<Text> ().text;
        string text3 = rand_answers.ElementAt (2).transform.GetChild (0).GetComponent<Text> ().text;

        Panels[2].transform.GetChild (0).transform.GetChild (0).GetComponent<Text> ().text = text1;
        Panels[2].transform.GetChild (1).transform.GetChild (0).GetComponent<Text> ().text = text2;
        Panels[2].transform.GetChild (2).transform.GetChild (0).GetComponent<Text> ().text = text3;
    }

    List<GameObject> Randomize_Answers (GameObject[] _answer_buttons)
    {
        List<GameObject> temp_button = _answer_buttons.ToList ();
        List<GameObject> shuffled_buttons = new List<GameObject> ();
        while (temp_button.Count > 0)
        {
            int random_counter = UnityEngine.Random.Range (0, temp_button.Count);
            GameObject _button = temp_button[random_counter];
            temp_button.RemoveAt (random_counter);
            shuffled_buttons.Add (_button);
        }
        return shuffled_buttons;
    }

    void Create_Answer_Function ()
    {
        for (int i = 0; i < answerButtons.Length; i++)
        {
            int save = i;
            answerButtons[i].GetComponent<Button> ().onClick.RemoveAllListeners ();
            answerButtons[i].GetComponent<Button> ().onClick.AddListener (() =>
            {
                Answer_Function (answerButtons[save]);
            });
        }
    }

    void Answer_Function (GameObject button)
    {
        if (answerPressed != true)
        {
            exitQuestionButton.GetComponent<Button> ().interactable = false;
            answerPressed = true;
            StartCoroutine (Change_Answers (button, button.transform.GetChild (0).GetComponent<Text> ().text));
        }
    }

    IEnumerator Change_Answers (GameObject _button, string pressed_a)
    {
        nextQuestionButton.SetActive (false);
        Text counter_text = Panels[4].transform.GetChild (0).GetComponent<Text> ();
        Image changeable = _button.GetComponent<Image> ();

        if (pressed_a == currentQuestionAnswer)
        {
            Set_Slider_Value (themeName);
            List<Question> temp = blockedThemes[themeId].questions.ToList ();

            for (int q = 0; q < temp.Count; q++)
            {
                if (temp[q].id == questionId)
                {
                    // Debug.Log (temp[q].id);
                    // Debug.Log (questionId);
                    temp.RemoveAt (q);
                }
            }

            blockedThemes[themeId].questions = temp.ToArray ();
            GameManager.instance.blockedThemesAndQuestions[themeName].Add (questionId);
            changeable.sprite = resources[1];
        }
        else
        {
            List<Question> temp = blockedThemes[themeId].questions.ToList ();

            for (int q = 0; q < temp.Count; q++)
            {
                if (temp[q].id == questionId)
                {
                    // Debug.Log (temp[q].id);
                    // Debug.Log (questionId);
                    temp.RemoveAt (q);
                }
            }

            blockedThemes[themeId].questions = temp.ToArray ();
            GameManager.instance.blockedThemesAndQuestions[themeName].Add (questionId);
            for (int i = 0; i < answerButtons.Length; i++)
            {
                if (answerButtons[i].transform.GetChild (0).GetComponent<Text> ().text == currentQuestionAnswer)
                {
                    Image change = answerButtons[i].GetComponent<Image> ();
                    change.sprite = resources[1];
                    break;
                }
            }
            changeable.sprite = resources[2];

        }

        GameManager.instance.totalAnsweredQuestions += 1;
        counter_text.text = GameManager.instance.blockedThemesAndQuestions[themeName].Count.ToString ();

        nextQuestionButton.GetComponent<Button> ().interactable = true;
        while (currentQuestion.fact == "")
        {
            currentQuestion = Get_Question (Get_Theme (selectedObjectTheme));
        }

        yield return new WaitForSeconds (0.1f);

        for (int i = 0; i < answerButtons.Length; i++)
        {
            answerButtons[i].GetComponent<Button> ().interactable = false;
        }

        if (GameManager.instance.blockedThemesAndQuestions[selectedObjectTheme].Count < GameManager.instance.maxQuestionsPerTheme)
        {
            nextQuestionButton.transform.GetChild (0).GetComponent<Text> ().text = "Nākamais jautājums";
        }
        else
        {
            nextQuestionButton.transform.GetChild (0).GetComponent<Text> ().text = "Aizvērt";
        }

        hintButton.GetComponent<Button> ().interactable = false;
        yield return new WaitForSeconds (1f);

        Text text_field = Panels[6].transform.GetChild (1).GetComponent<Text> ();
        text_field.text = currentQuestion.fact;

        quizAnimator.SetTrigger ("Load Fact");

        yield return new WaitForSeconds (2f);
        changeable.sprite = resources[0];
        for (int i = 0; i < answerButtons.Length; i++)
        {
            if (answerButtons[i].transform.GetChild (0).GetComponent<Text> ().text == currentQuestionAnswer)
            {
                Image change = answerButtons[i].GetComponent<Image> ();
                change.sprite = resources[0];
                break;
            }
        }
        nextQuestionButton.SetActive (true);
    }

    public void next_question ()
    {
        // Debug.Log (GameManager.instance.totalAnsweredQuestions);
        if (GameManager.instance.blockedThemesAndQuestions[selectedObjectTheme].Count < GameManager.instance.maxQuestionsPerTheme)
        {
            quizAnimator.SetTrigger ("Exit Fact");

            Question new_question = Get_Question (Get_Theme (selectedObjectTheme));
            Create_Question (new_question);

            quizAnimator.SetTrigger ("Load Question");

        }
        else
        {
            quizAnimator.SetTrigger ("Exit Fact Only");

            for (int i = 0; i < answerButtons.Length; i++)
            {
                answerButtons[i].GetComponent<Button> ().interactable = true;
                Image changeable = answerButtons[i].GetComponent<Image> ();
                changeable.sprite = resources[0];
            }
            GameManager.instance.questionLoaded = false;
        }

        if (GameManager.instance.totalAnsweredQuestions == GameManager.instance.totalAllowedQuestions)
        {
            Panels[5].transform.parent.GetChild (1).gameObject.SetActive (false);
            Panels[0].SetActive (false);
            Time.timeScale = 0;
            int childs = Panels[5].transform.GetChild (0).childCount;
            while (Panels[5].transform.GetChild (0).childCount != 0)
            {
                childs -= 1;
                RectTransform rt_panel = Panels[9].transform.GetChild (0).GetComponent<RectTransform> ();
                rt_panel.GetComponent<GridLayoutGroup> ().cellSize = new Vector2 ((rt_panel.rect.width - 20) / 2, (rt_panel.rect.height) / 5);
                Panels[5].transform.GetChild (0).GetChild (childs).transform.SetParent (Panels[9].transform.GetChild (0).transform, false);
            }

            Panels[9].transform.GetChild (3).GetComponent<Text> ().text = ((Math.Round (GameManager.instance.totalScore * 100) / 2)).ToString () + " no 50 jautājumiem";
            Panels[9].SetActive (true);
        }

        for (int i = 0; i < answerButtons.Length; i++)
        {
            answerButtons[i].GetComponent<Button> ().interactable = true;
            Image changeable = answerButtons[i].GetComponent<Image> ();
            changeable.sprite = resources[0];
        }

        hintButton.GetComponent<Button> ().interactable = true;

        hintPressed = false;
        answerPressed = false;
        nextQuestionPressed = false;
        quizAnimator.ResetTrigger ("Load Question");
    }

    IEnumerator Draw_Effect_Panel (GameObject panel, float duration = 0.7f, float end_time = 1f)
    {

        panel.transform.GetChild (2).transform.GetChild (0).GetComponent<Text> ().text = currentQuestion.hint;

        for (int i = 0; i < panel.transform.childCount; i++)
        {

            // Cretate child panel object for shorter acces
            GameObject child_panel = panel.transform.GetChild (i).gameObject;
            CanvasGroup _alpha = child_panel.GetComponent<CanvasGroup> ();

            float t = 0;

            while (t < duration)
            {
                // Step the fade forward one frame.
                t += Time.deltaTime;
                // Turn the time into an interpolation factor between 0 and 1.
                float blend = Mathf.Clamp01 (t / (duration / 2f));
                // Blend to the corresponding opacity between start & target.
                _alpha.alpha = Mathf.Lerp (0f, 1f, blend);
                // Wait one frame, and repeat.
                yield return null;
            }
        }
    }

    void Set_Slider_Value (string _current_theme)
    {
        GameManager.instance.totalScore += 0.02f;
        for (int theme_id = 0; theme_id < GameManager.instance.all_statistics.Count; theme_id++)
        {
            string stat_name = GameManager.instance.all_statistics[theme_id].transform.name;
            if (stat_name == _current_theme)
            {
                GameManager.instance.all_statistics.ElementAt (theme_id).transform.GetChild (2).GetComponent<Slider> ().value += 0.2f;
                float fill = GameManager.instance.all_statistics.ElementAt (theme_id).transform.GetChild (2).GetComponent<Slider> ().value;
                GameManager.instance.all_statistics.ElementAt (theme_id).transform.GetChild (1).GetComponent<Text> ().text = (fill * 100).ToString () + "%";
                change_total_score ();
            }
        }
    }

    void change_total_score ()
    {
        Transform total_panel = Panels[5].transform.GetChild (1);
        total_panel.GetChild (0).transform.GetChild (0).GetComponent<Image> ().fillAmount = GameManager.instance.totalScore;
    }

    public void SetZoom ()
    {
        cameraZoom.orthographicSize = zoomSlider.value;
    }

    public void SetVolume(Slider volumeSlider)
    {
        AudioSource audio = GameManager.instance.gameObject.GetComponent<AudioSource>();
        audio.volume = volumeSlider.value;
    }

}