﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class Quiz_List {
	public Theme[] themes;
}

[System.Serializable]
public class Theme {
	public string name;
	public Question[] questions;
}

[System.Serializable]
public class List_Theme {
	public string name;
	public List<Question> questions = new List<Question> ();
}

[System.Serializable]
public class Question {
	public int id;
	public string hint;
	public string fact;
	public string question;
	public string[] answers;
	public int right_answer;
}