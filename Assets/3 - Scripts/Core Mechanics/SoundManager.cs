﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour {

	public AudioSource audioSource;

	public AudioClip[] soundResources;

	void Awake () {
		// audioSource = GetComponent<AudioSource> ();
	}

	// Use this for initialization
	void Start () {

		string currentScene = SceneManager.GetActiveScene ().name;

		if (currentScene == "CITY_LEVEL") {
			audioSource.PlayOneShot (soundResources[0], 0.5f);
		};

		// play music
		// audioSource.clip = soundResources[2];
		// audioSource.PlayOneShot (soundResources[1], 0.2f);
	}

	// Update is called once per frame
	void Update () {

	}

	public void onButtonClicked () {
		audioSource.loop = false;
		audioSource.clip = soundResources[2];
		audioSource.Play ();
		// audioSource.PlayOneShot (soundResources[2], 1f);
	}
}