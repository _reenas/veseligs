﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToCity : MonoBehaviour {

	public Button cityButton;

	// Use this for initialization
	void Start () {
		cityButton.onClick.AddListener (() => {
			GameManager.instance.changeScene ("CITY_LEVEL", "City/");
		});
	}

	// Update is called once per frame
	void Update () {
		
	}
}