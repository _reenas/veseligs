﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityManager : MonoBehaviour
{

    public static CityManager instance;
    public GameObject player;

    public enum City
    {
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE
    }
    public City currentBlock;
    public GameObject[] CityBlocks;

    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        currentBlock = City.ONE;
    }

    public void BlockCheck(GameObject block)
    {
        City blockType = (City)System.Enum.Parse(typeof(City), block.transform.name);
        currentBlock = blockType;
        UpdateCity();
    }

    void UpdateCity()
    {
        switch (currentBlock)
        {
            case (City.ONE):
                CityBlocks[0].SetActive(true);
                CityBlocks[1].SetActive(false);
                CityBlocks[2].SetActive(false);
                CityBlocks[3].SetActive(false);
                CityBlocks[4].SetActive(false);
                break;
            case (City.TWO):
                CityBlocks[0].SetActive(false);
                CityBlocks[1].SetActive(true);
                CityBlocks[2].SetActive(false);
                CityBlocks[3].SetActive(false);
                CityBlocks[4].SetActive(false);
                break;
            case (City.THREE):
                CityBlocks[0].SetActive(false);
                CityBlocks[1].SetActive(false);
                CityBlocks[2].SetActive(true);
                CityBlocks[3].SetActive(false);
                CityBlocks[4].SetActive(false);
                break;
            case (City.FOUR):
                CityBlocks[0].SetActive(false);
                CityBlocks[1].SetActive(false);
                CityBlocks[2].SetActive(false);
                CityBlocks[3].SetActive(true);
                CityBlocks[4].SetActive(false);
                break;
            case (City.FIVE):
                CityBlocks[0].SetActive(false);
                CityBlocks[1].SetActive(false);
                CityBlocks[2].SetActive(false);
                CityBlocks[3].SetActive(false);
                CityBlocks[4].SetActive(true);
                break;
        }
    }
}