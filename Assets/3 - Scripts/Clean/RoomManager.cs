﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
    public static RoomManager instance;
    public GameObject player;

    public enum Rooms
    {
        LIVING,
        CORRIDOR,
        KITCHEN,
        BATHROOM
    }
    public Rooms currentRoom;

    public GameObject koridors;
    public GameObject virtuve;
    public GameObject vannasIstaba;
    public GameObject dzivojamaIstaba;
    public GameObject fadingWall;

    public GameObject[] arrows;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        currentRoom = Rooms.LIVING;
    }

    public void RoomCheck(GameObject room)
    {
        Rooms roomType = (Rooms)System.Enum.Parse(typeof(Rooms), room.transform.name);
        currentRoom = roomType;
        UpdateRoom();
    }
    public void WallFade(GameObject wall)
    {
        fadingWall = wall;
        wall.GetComponent<MeshRenderer>().enabled = false;
    }
    void UpdateRoom()
    {
        switch (currentRoom)
        {
            case (Rooms.LIVING):
                dzivojamaIstaba.SetActive(true);
                koridors.SetActive(false);
                vannasIstaba.SetActive(false);
                virtuve.SetActive(false);
                if (fadingWall != null) fadingWall.GetComponent<MeshRenderer>().enabled = true;
                arrows[0].GetComponent<BoxCollider>().enabled = true;
                arrows[0].GetComponent<MeshRenderer>().enabled = true;
                arrows[1].GetComponent<BoxCollider>().enabled = false;
                arrows[1].GetComponent<MeshRenderer>().enabled = false;
                arrows[2].GetComponent<BoxCollider>().enabled = false;
                arrows[2].GetComponent<MeshRenderer>().enabled = false;
                arrows[3].GetComponent<BoxCollider>().enabled = false;
                arrows[3].GetComponent<MeshRenderer>().enabled = false;
                arrows[4].GetComponent<BoxCollider>().enabled = false;
                arrows[4].GetComponent<MeshRenderer>().enabled = false;
                arrows[5].GetComponent<BoxCollider>().enabled = false;
                arrows[5].GetComponent<MeshRenderer>().enabled = false;
                break;

            case (Rooms.CORRIDOR):
                dzivojamaIstaba.SetActive(false);
                koridors.SetActive(true);
                vannasIstaba.SetActive(false);
                virtuve.SetActive(false);
                if (fadingWall != null) fadingWall.GetComponent<MeshRenderer>().enabled = true;
                arrows[0].GetComponent<BoxCollider>().enabled = false;
                arrows[0].GetComponent<MeshRenderer>().enabled = false;
                arrows[1].GetComponent<BoxCollider>().enabled = false;
                arrows[1].GetComponent<MeshRenderer>().enabled = false;
                arrows[2].GetComponent<BoxCollider>().enabled = false;
                arrows[2].GetComponent<MeshRenderer>().enabled = false;
                arrows[3].GetComponent<BoxCollider>().enabled = true;
                arrows[3].GetComponent<MeshRenderer>().enabled = true;
                arrows[4].GetComponent<BoxCollider>().enabled = true;
                arrows[4].GetComponent<MeshRenderer>().enabled = true;
                arrows[5].GetComponent<BoxCollider>().enabled = true;
                arrows[5].GetComponent<MeshRenderer>().enabled = true;

                break;

            case (Rooms.KITCHEN):
                dzivojamaIstaba.SetActive(false);
                koridors.SetActive(false);
                vannasIstaba.SetActive(false);
                virtuve.SetActive(true);
                if (fadingWall != null) fadingWall.GetComponent<MeshRenderer>().enabled = true;
                arrows[0].GetComponent<BoxCollider>().enabled = false;
                arrows[0].GetComponent<MeshRenderer>().enabled = false;
                arrows[1].GetComponent<BoxCollider>().enabled = true;
                arrows[1].GetComponent<MeshRenderer>().enabled = true;
                arrows[2].GetComponent<BoxCollider>().enabled = false;
                arrows[2].GetComponent<MeshRenderer>().enabled = false;
                arrows[3].GetComponent<BoxCollider>().enabled = false;
                arrows[3].GetComponent<MeshRenderer>().enabled = false;
                arrows[4].GetComponent<BoxCollider>().enabled = false;
                arrows[4].GetComponent<MeshRenderer>().enabled = false;
                arrows[5].GetComponent<BoxCollider>().enabled = false;
                arrows[5].GetComponent<MeshRenderer>().enabled = false;
                break;

            case (Rooms.BATHROOM):
                dzivojamaIstaba.SetActive(false);
                koridors.SetActive(false);
                vannasIstaba.SetActive(true);
                virtuve.SetActive(false);
                if (fadingWall != null) fadingWall.GetComponent<MeshRenderer>().enabled = true;
                arrows[0].GetComponent<BoxCollider>().enabled = false;
                arrows[0].GetComponent<MeshRenderer>().enabled = false;
                arrows[1].GetComponent<BoxCollider>().enabled = false;
                arrows[1].GetComponent<MeshRenderer>().enabled = false;
                arrows[2].GetComponent<BoxCollider>().enabled = true;
                arrows[2].GetComponent<MeshRenderer>().enabled = true;
                arrows[3].GetComponent<BoxCollider>().enabled = false;
                arrows[3].GetComponent<MeshRenderer>().enabled = false;
                arrows[4].GetComponent<BoxCollider>().enabled = false;
                arrows[4].GetComponent<MeshRenderer>().enabled = false;
                arrows[5].GetComponent<BoxCollider>().enabled = false;
                arrows[5].GetComponent<MeshRenderer>().enabled = false;
                break;
        }
    }
}
