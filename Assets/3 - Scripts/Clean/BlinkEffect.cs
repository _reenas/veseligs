﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkEffect : MonoBehaviour
{

    private Renderer _renderer;
    public Color _color;
    private Color _color2;
    private Color local_color;
    float lerp;
    bool blink;

    void Start()
    {
        // // TODO: Add effect for all children with renderer component
        // if (transform.childCount > 2) _renderer = transform.GetChild(1).GetComponent<Renderer>();
        // else 
        _renderer = GetComponent<Renderer>();
        local_color = _renderer.material.color;
        blink = false;
        _color2 = _color;
    }

    void Update()
    {
        if (blink)
            Blink();
    }

    void Blink()
    {
        float duration = 0.5f;
        lerp = Mathf.PingPong(Time.time, duration);
        _renderer.material.color = Color.Lerp(Color.white, _color, lerp);

    }
    void OnMouseEnter()
    {
        _color = _color2;
        blink = true;
    }
    void OnMouseExit()
    {
        _color.r = 1f;
        _color.g = 1f;
        _color.b = 1f;
        blink = false;
        _renderer.material.color = local_color;
    }
}