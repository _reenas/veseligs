﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallFade : MonoBehaviour
{
    void OnMouseOver()
    {
        if(!GameManager.instance.questionLoaded) {
            RoomManager.instance.WallFade(this.gameObject);
        }
    }
    
    void OnMouseExit()
    {
        this.gameObject.GetComponent<MeshRenderer>().enabled = true;
    }
}
