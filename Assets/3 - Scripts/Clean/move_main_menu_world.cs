﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class move_main_menu_world : MonoBehaviour{
	public float length;
	public float time;
	public float yFix;

	public void Update () {
		transform.position = new Vector3 (transform.position.x, transform.position.y, Mathf.PingPong (time * Time.time, length) - yFix);
	}

}