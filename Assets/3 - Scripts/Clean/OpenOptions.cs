﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenOptions : MonoBehaviour {
	public GameObject optionsPanel;
	bool change = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			change = !change;
			optionsPanel.SetActive(change);
		}
	}
}
