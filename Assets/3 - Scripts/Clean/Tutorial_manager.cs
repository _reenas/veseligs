﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Tutorial_manager : MonoBehaviour {
	public GameObject[] image;
	public GameObject nextBtn;
	public Animator anim;
	public int curImg;
	public GameObject arrow;

	void Start () {
		for (int i = 0; i < image.Length; i++) {
			image[i].SetActive (false);
		}
		nextBtn.SetActive (false);
		arrow.SetActive (false);
		curImg = 0;
		StartCoroutine (Tutorial ());
	}

	IEnumerator Tutorial () {
		anim.SetTrigger ("start");
		yield return new WaitForSeconds (1f);

	}

	public void Next () {
		if (curImg < 5) {
			image[curImg].SetActive (false);
			image[curImg + 1].SetActive (true);
			curImg++;
			if (curImg == 1) {
				arrow.SetActive (true);
				anim.SetTrigger ("arrow1");
			}
			if (curImg == 2) {
				anim.SetTrigger ("arrow2");
			}
			if (curImg == 3) {
				anim.SetTrigger ("arrow3");
			}
			if (curImg == 4) {
				anim.SetTrigger ("arrow4");
			}
			if (curImg == 5) {
				arrow.SetActive (false);
			}
		} else {
			SceneManager.LoadSceneAsync ("MAIN_MENU");
			nextBtn.SetActive (false);
		}
	}
}