﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHover : MonoBehaviour
{
    public float length;
    public float time;
    public float yFix;

    void Update()
    {
        transform.position = new Vector3(transform.position.x, Mathf.PingPong(time * Time.time, length) - yFix, transform.position.z);
    }
}
