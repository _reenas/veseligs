﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class hover_effect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public void OnPointerEnter (PointerEventData pointerEventData) {
		float newscale = 1.1f;
		this.GetComponent<RectTransform> ().localScale = new Vector3 (newscale, newscale, 1f);
	}

	public void OnPointerExit (PointerEventData pointerEventData) {

		this.GetComponent<RectTransform> ().localScale = new Vector3 (1f, 1f, 1f);
	}
}