﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToMenu : MonoBehaviour {
	public GameObject stat_prefab;

	public void goToMain () {
		Time.timeScale = 1;
		GameManager.instance.Load_Themes(GameManager.instance.loaded_text);
		GameManager.instance.blockedThemesAndQuestions.Clear();
		GameManager.instance.totalAnsweredQuestions = 0;
		GameManager.instance.totalScore = 0;
		GameManager.instance.makeStats(stat_prefab);
		GameManager.instance.changeScene ("Main Menu", "Menu/");
	}
}