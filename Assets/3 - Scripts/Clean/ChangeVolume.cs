﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeVolume : MonoBehaviour {

	public Slider volumeSlider;
	AudioSource audio;
	// Use this for initialization
	void Start () {
		audio = GameManager.instance.gameObject.GetComponent<AudioSource> ();
		volumeSlider.value = audio.volume;
	}

	// Update is called once per frame
	void Update () {
		SetVolume();
	}

	public void SetVolume () {
		audio.volume = volumeSlider.value;
	}
}