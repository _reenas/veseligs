﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TrafficSystem : MonoBehaviour {
    public static TrafficSystem instance;

    public enum TrafficLightState {
        RED,
        YELLOW,
        GREEN
    }
    public TrafficLightState trafficState;
    public List<GameObject> cars;
    public List<GameObject> trafficSpawns;
    public List<GameObject> trafficDespawns;

    public GameObject[] trafficLightBlocks;

    [Header ("Luksafors")]
    public Material sarkans;
    public Material dzeltens;
    public Material zals;
    public Material melnss;
    //Luksafors 1
    public GameObject l1Sarkans;
    public GameObject l1Zals;
    public GameObject l1Dzeltens;
    public GameObject l1SarkansPareja;
    public GameObject l1ZalsPareja;
    //Luksafors 2
    public GameObject l2Sarkans;
    public GameObject l2Zals;
    public GameObject l2Dzeltens;
    public bool playerCrossed = false;
    public GameObject cross1;
    public GameObject cross2;
    public GameObject player;
    public GameObject link;
    public int route;
    

    public bool MoveAcrossNavMeshesStarted;

    void Start () {
        if (instance != null) {
            instance = this;
        }
        route = 0;
        InvokeRepeating ("SpawnCar", 0.5f, 30f);
        StartCoroutine (TrafficLights ());
        trafficState = TrafficLightState.GREEN;
        cross1.SetActive (false);
        cross2.SetActive (false);
        player = GameObject.Find ("PLAYER");
        link.SetActive (false);

    }

    void Update () {
        switch (trafficState) {
            case (TrafficLightState.RED):
                l1Sarkans.GetComponent<Renderer> ().material = melnss;
                l1Dzeltens.GetComponent<Renderer> ().material = melnss;
                l1Zals.GetComponent<Renderer> ().material = zals;
                l2Sarkans.GetComponent<Renderer> ().material = sarkans;
                l2Dzeltens.GetComponent<Renderer> ().material = melnss;
                l2Zals.GetComponent<Renderer> ().material = melnss;

                l1SarkansPareja.GetComponent<Renderer> ().material = sarkans;
                l1ZalsPareja.GetComponent<Renderer> ().material = melnss;
                break;
            case (TrafficLightState.YELLOW):
                l1Sarkans.GetComponent<Renderer> ().material = melnss;
                l1Dzeltens.GetComponent<Renderer> ().material = dzeltens;
                l1Zals.GetComponent<Renderer> ().material = melnss;
                l2Sarkans.GetComponent<Renderer> ().material = melnss;
                l2Dzeltens.GetComponent<Renderer> ().material = dzeltens;
                l2Zals.GetComponent<Renderer> ().material = melnss;

                l1SarkansPareja.GetComponent<Renderer> ().material = sarkans;
                l1ZalsPareja.GetComponent<Renderer> ().material = melnss;

                cross1.SetActive (false);
                cross2.SetActive (false);
                break;
            case (TrafficLightState.GREEN):
                l1Sarkans.GetComponent<Renderer> ().material = sarkans;
                l1Dzeltens.GetComponent<Renderer> ().material = melnss;
                l1Zals.GetComponent<Renderer> ().material = melnss;
                l2Sarkans.GetComponent<Renderer> ().material = melnss;
                l2Dzeltens.GetComponent<Renderer> ().material = melnss;
                l2Zals.GetComponent<Renderer> ().material = zals;

                l1SarkansPareja.GetComponent<Renderer> ().material = melnss;
                l1ZalsPareja.GetComponent<Renderer> ().material = zals;

                if (!playerCrossed) {
                    cross1.SetActive (true);
                    cross2.SetActive (false);
                } else if (playerCrossed) {
                    cross2.SetActive (true);
                    cross1.SetActive (false);
                }
                if (Input.GetMouseButtonDown (0)) {
                    Camera cam = GameObject.Find ("CAMERA").GetComponent<Camera> ();
                    Ray ray = cam.ScreenPointToRay (Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
                        if (hit.transform.gameObject == cross1 || hit.transform.gameObject == cross2) {
                            CrossStreet ();
                        }
                    }
                }
                break;
        }
    }

    public void CrossStreet () {
        Debug.Log ("Pressed");
        // PlayerManager.instance.can_move_player = false;
        Transform cross1point = GameObject.Find ("Cross1Point").transform;
        Transform cross2point = GameObject.Find ("Cross2Point").transform;

        if (playerCrossed) {
            StartCoroutine (StreetCrossing (cross1point, false));

        } else if (!playerCrossed) {

            StartCoroutine (StreetCrossing (cross2point, true));
        }
    }
    IEnumerator StreetCrossing (Transform point, bool crossed) {
        link.SetActive (true);

        PlayerManager.instance.motor.MoveToPoint (point.position);
        yield return new WaitForSeconds (4f);

        PlayerManager.instance.can_move_player = true;
        link.SetActive (false);

    }

    IEnumerator TrafficLights () {
        trafficLightBlocks[0].SetActive (false);
        trafficLightBlocks[1].SetActive (false);
        trafficLightBlocks[2].SetActive (true);
        yield return new WaitForSeconds (10f);
        trafficState = TrafficLightState.YELLOW;
        yield return new WaitForSeconds (2f);
        trafficState = TrafficLightState.RED;
        trafficLightBlocks[0].SetActive (true);
        trafficLightBlocks[1].SetActive (true);
        trafficLightBlocks[2].SetActive (false);
        yield return new WaitForSeconds (8f);
        trafficState = TrafficLightState.YELLOW;
        yield return new WaitForSeconds (2f);
        trafficState = TrafficLightState.GREEN;
        StartCoroutine (TrafficLights ());
    }

    void SpawnCar () {
        //Choose spawner
        int spawnNr = route;
        route++;
        if (route == 2) route = 0;
        GameObject car = Instantiate (cars[Random.Range (0, cars.Count)], trafficSpawns[spawnNr].transform.position, spawnNr == 0 ? Quaternion.Euler (0, -90f, 0) : Quaternion.Euler (0, 90f, 0));
        car.transform.name = "car" + spawnNr;
        car.GetComponent<CarController> ().route = spawnNr;
    }
}