﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {
    public enum CarState {
        MOVE,
        STOP
    }
    public enum CarRoute {
        ONE,
        TWO
    }
    public CarState carState;
    public CarRoute carRoute;

    public Transform[] path;
    public List<Transform> nodes;
    public int currentNode = 0;

    public int pathSelect;

    [HideInInspector]
    public int route;

    //Car movement
    public WheelCollider wheelFL;
    public WheelCollider wheelFR;
    public WheelCollider wheelRL;
    public WheelCollider wheelRR;
    public float maxSteeringAngle = 45f;
    public float maxMotorTorque = 100f;
    public float maxBrakeTorque = 400f;
    public float currentSpeed;
    public float maxSpeed = 120f;
    public Vector3 centerOfMass;
    public bool isBraking;

    [Header ("Sensors")]
    public float sensorLength = 5f;
    public float frontSideSensorPosition = 0.2f;
    public Vector3 frontSensorPosition = new Vector3 (0f, 0.2f, 0.5f);
    public float frontSensorAngle = 30f;
    public bool somethingsInFront = false;

    void Start () {
        Physics.IgnoreLayerCollision (0, 30);
        GetComponent<Rigidbody> ().centerOfMass = centerOfMass;

        path[0] = GameObject.Find ("Path1").transform;
        path[1] = GameObject.Find ("Path2").transform;

        carState = CarState.MOVE;

        if (route == 0) {
            pathSelect = 0;
            carRoute = CarRoute.ONE;

            Transform[] pathTransforms = path[pathSelect].GetComponentsInChildren<Transform> ();
            nodes = new List<Transform> ();

            for (int i = 1; i < pathTransforms.Length; i++) {
                if (pathTransforms[i] != transform) {
                    nodes.Add (pathTransforms[i]);
                }
            }
        } else if (route == 1) {
            pathSelect = 1;
            carRoute = CarRoute.TWO;

            Transform[] pathTransforms = path[pathSelect].GetComponentsInChildren<Transform> ();
            nodes = new List<Transform> ();

            for (int i = 1; i < pathTransforms.Length; i++) {
                if (pathTransforms[i] != transform) {
                    nodes.Add (pathTransforms[i]);
                }
            }
        }
    }

    void FixedUpdate () {
        CheckWaypointDistance ();
        Braking ();
        Sensors ();
        switch (carState) {
            case (CarState.MOVE):
                isBraking = false;
                ApplySteer ();
                Drive ();

                break;
            case (CarState.STOP):
                isBraking = true;
                break;
        }
    }

    private void ApplySteer () {
        Vector3 relativeVector = transform.InverseTransformPoint (nodes[currentNode].position);
        float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteeringAngle;
        wheelFL.steerAngle = newSteer;
        wheelFR.steerAngle = newSteer;
    }
    private void Drive () {

        currentSpeed = 2 * Mathf.PI * wheelFL.radius * wheelFL.rpm * 60 / 10000;

        if (currentSpeed < maxSpeed && !isBraking) {
            wheelFL.motorTorque = maxMotorTorque;
            wheelFR.motorTorque = maxMotorTorque;
        } else {
            wheelFL.motorTorque = 0;
            wheelFR.motorTorque = 0;
        }
    }

    private void CheckWaypointDistance () {
        // Debug.Log (Vector3.Distance (transform.position, nodes[currentNode].position));
        if (Vector3.Distance (transform.position, nodes[currentNode].position) < 0.7f) {

            if (currentNode == nodes.Count - 1) {
                currentNode = 0;
            } else {
                currentNode++;
            }
        }
    }

    private void Braking () {
        if (isBraking) {
            wheelRL.brakeTorque = maxBrakeTorque;
            wheelRR.brakeTorque = maxBrakeTorque;
        } else {
            wheelRL.brakeTorque = 0;
            wheelRR.brakeTorque = 0;
        }
    }
    private void Sensors () {
        RaycastHit hit;
        Vector3 sensorStartPos = transform.position;
        sensorStartPos += transform.forward * frontSensorPosition.z;
        sensorStartPos += transform.up * frontSensorPosition.y;
        //

        //front center sensor
        if (Physics.Raycast (sensorStartPos, transform.forward, out hit, sensorLength)) {
            if (hit.collider.gameObject.tag == "Car" || hit.collider.gameObject.tag == "Block" || hit.collider.gameObject.tag == "Player") {
                carState = CarState.STOP;
                // Debug.DrawLine (sensorStartPos, hit.point);
                somethingsInFront = true;
            } else if (somethingsInFront) {
                somethingsInFront = false;
            }
        } else if (somethingsInFront == true) {
            somethingsInFront = false;
            carState = CarState.MOVE;
        }
        //front right sensor
        sensorStartPos += transform.right * frontSideSensorPosition;
        if (Physics.Raycast (sensorStartPos, transform.forward, out hit, sensorLength)) {
            if (hit.collider.gameObject.tag == "Car" || hit.collider.gameObject.tag == "Block" || hit.collider.gameObject.tag == "Player") {
                carState = CarState.STOP;
                // Debug.DrawLine (sensorStartPos, hit.point);
                somethingsInFront = true;
            } else if (somethingsInFront) {
                somethingsInFront = false;
            }
        } else if (somethingsInFront == true) {
            somethingsInFront = false;
            carState = CarState.MOVE;
        }
        //front right angle sensor
        if (Physics.Raycast (sensorStartPos, Quaternion.AngleAxis (frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
            // Debug.DrawLine (sensorStartPos, hit.point);
        }
        //front left sensor
        sensorStartPos -= transform.right * frontSideSensorPosition * 2;
        if (Physics.Raycast (sensorStartPos, transform.forward, out hit, sensorLength)) {
            if (hit.collider.gameObject.tag == "Car" || hit.collider.gameObject.tag == "Block" || hit.collider.gameObject.tag == "Player") {
                carState = CarState.STOP;
                // Debug.DrawLine (sensorStartPos, hit.point);
                somethingsInFront = true;
            } else if (somethingsInFront) {
                somethingsInFront = false;
            }
        } else if (somethingsInFront == true) {
            somethingsInFront = false;
            carState = CarState.MOVE;
        }
        //front left angle sensor
        if (Physics.Raycast (sensorStartPos, Quaternion.AngleAxis (-frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
            // Debug.DrawLine (sensorStartPos, hit.point);
        }
    }

    void OnTriggerEnter (Collider other) {
        if (other.transform.tag == "CarDespawn") {
            Destroy (this.gameObject);
        }
    }
}